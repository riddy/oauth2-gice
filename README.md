# GSF GICE OAuth 2.0 Client

This package provides GICE OAuth 2.0 support for the PHP League's [OAuth 2.0 Client](https://github.com/thephpleague/oauth2-client).

## Installation

To install, use composer:

```
composer require riddy/oauth2-gice
```

## Testing

Not working cause meh

## Credits

Riddy Zanguard ([Gitlab](https://gitlab.com/riddy/), [Github](https://github.com/riddym))

## License

The MIT License (MIT). Please see [License File](https://github.com/evelabs/oauth2-eveonline/blob/master/LICENSE) for more information.
