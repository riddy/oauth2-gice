<?php

namespace GICE\OAuth2\Client\Provider;

use League\OAuth2\Client\Provider\GenericResourceOwner;

class GICEResourceOwner extends GenericResourceOwner
{
    /**
     * Raw response
     *
     * @var array
     */
    protected $response;

    /**
     * @return string|int
     */
    public function getId()
    {
        return $this->response["sub"];
    }

    /**
     * @return string
     */
    public function getUsername()
    {
        return $this->response["username"];
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->response["name"];
    }

    /**
     * @return string|int
     */
    public function getGroup()
    {
        return $this->response["pri_grp"];
    }

    /**
     * Return all of the owner details available as an array.
     *
     * @return array
     */
    public function toArray()
    {
        return $this->response;
    }
}
